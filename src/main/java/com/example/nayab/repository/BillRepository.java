package com.example.nayab.repository;

import com.example.nayab.domain.MyOrder;
import org.springframework.data.repository.CrudRepository;

public interface BillRepository extends CrudRepository<MyOrder,Integer> {
}
