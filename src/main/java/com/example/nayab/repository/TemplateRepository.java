package com.example.nayab.repository;

import com.example.nayab.domain.OrderItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends CrudRepository<OrderItem,Integer> {
}
