package com.example.nayab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NayabApplication {

	public static void main(String[] args) {
		SpringApplication.run(NayabApplication.class, args);
	}
}
