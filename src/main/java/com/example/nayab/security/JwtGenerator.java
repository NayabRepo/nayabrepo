package com.example.nayab.security;

import com.example.nayab.domain.User;
import com.example.nayab.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

@Component
public class JwtGenerator {

    public String generate(User user){

        Claims claims= Jwts.claims()
                .setSubject(user.getName());
        claims.put("userId",user.getUId());
//        claims.put("role",jwtUser.getRole());
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512,"nayab")
        .compact();
    }
}
