package com.example.nayab.security;

import com.example.nayab.model.JwtAuthenticationToken;
import com.example.nayab.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class JwtValidator {

    private String secret="nayab";

    public JwtUser validate(String  jwtAuthentationToken) {
        JwtUser jwtUser = null;
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(jwtAuthentationToken)
                    .getBody();

            jwtUser = new JwtUser();
            jwtUser.setUserName(body.getSubject());
            jwtUser.setId(Long.parseLong((String) body.get("userId")));
            jwtUser.setRole((String) body.get("role"));

        } catch (Exception e) {
            System.out.println(e);
        }

        return jwtUser;
    }
}
