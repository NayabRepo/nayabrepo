package com.example.nayab.controller;

import com.example.nayab.domain.User;
import com.example.nayab.service.Restaurants.SearchRestaurants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/restaurants")
public class Restaurants {

    @Autowired
    private SearchRestaurants searchRestaurants;

    @GetMapping("/search")
    ResponseEntity<?> login(@RequestHeader("Authentication") String token){
        return searchRestaurants.listAllRestaurants();
    }
}
