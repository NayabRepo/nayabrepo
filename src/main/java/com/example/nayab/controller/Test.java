package com.example.nayab.controller;

import com.example.nayab.domain.MyOrder;
import com.example.nayab.domain.OrderItem;
import com.example.nayab.model.JwtUser;
import com.example.nayab.security.JwtGenerator;
import com.example.nayab.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1/test")
@RestController
public class Test {

    @Autowired
    private TestService testService;

    @Autowired
    private JwtGenerator jwtGenerator;

    @PostMapping("/orderItem")
    ResponseEntity<?> saveTemplate(@RequestBody OrderItem orderItem){
       if(orderItem !=null)
        return testService.saveTemplate(orderItem);
       else
           return new ResponseEntity<>("Invalid Obj", HttpStatus.BAD_REQUEST);

    }

    @PostMapping("/order")
    ResponseEntity<?> saveBill(@RequestBody JwtUser jwtUser){
        System.out.println("hdfghkj");
        return new ResponseEntity<>(jwtGenerator.generate(jwtUser),HttpStatus.OK);


    }
}
