package com.example.nayab.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/alerts")
public class Alerts {

    private static final Logger log = LogManager.getLogger(Alerts.class);


    /**
     * Fetch all pending request under that supervisor empCode
     * @param empCode
     * @return
     */
    @GetMapping("/pendingRequest/{empCode}")
    ResponseEntity<?> getPendingRequest(@PathVariable("empCode") String empCode) {
        log.info("Entering Controller Class ::: Alerts ::: method ::: getPendingRequest");

        return new ResponseEntity<>("EmpCode can't null", HttpStatus.OK);
    }
}
