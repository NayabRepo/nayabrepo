package com.example.nayab.controller;

import com.example.nayab.domain.MyOrder;
import com.example.nayab.domain.User;
import com.example.nayab.model.JwtUser;
import com.example.nayab.security.JwtGenerator;
import com.example.nayab.service.TestService;
import com.example.nayab.service.UserService;
import com.example.nayab.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1")
@RestController
public class Authenticate {

    @Autowired
    private JwtGenerator jwtGenerator;

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    ResponseEntity<?> login(@RequestBody User user){
       return userService.login(user);
    }

    @PostMapping("/signup")
    ResponseEntity<?> sigup(@RequestBody User user){
        return userService.signup(user);
    }
}
