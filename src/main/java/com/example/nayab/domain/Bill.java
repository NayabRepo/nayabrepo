package com.example.nayab.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    private String cName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "templateId",nullable = false)
    private Template template;


}
