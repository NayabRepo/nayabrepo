package com.example.nayab.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String orderName;

    @ManyToOne
    @JoinColumn(name = "fk_order")
    private MyOrder myOrder;

}
