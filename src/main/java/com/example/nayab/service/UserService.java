package com.example.nayab.service;

import com.example.nayab.domain.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    public ResponseEntity<?> signup(User user);

    public Boolean validateUser(User user);

    public ResponseEntity<?> login(User user);
}
