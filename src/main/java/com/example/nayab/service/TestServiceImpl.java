package com.example.nayab.service;

import com.example.nayab.domain.MyOrder;
import com.example.nayab.domain.OrderItem;
import com.example.nayab.repository.BillRepository;
import com.example.nayab.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    TemplateRepository testRepository;
    @Autowired
    BillRepository billRepository;

    @Override
    public ResponseEntity<?> saveTemplate(OrderItem orderItem) {
        testRepository.save(orderItem);
        return new ResponseEntity<>("Saved Successfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> saveBill(MyOrder order) {

        billRepository.save(order);
        return new ResponseEntity<>("Saved Successfully", HttpStatus.OK);
    }


}
