package com.example.nayab.service;

import com.example.nayab.domain.MyOrder;
import com.example.nayab.domain.OrderItem;
import org.springframework.http.ResponseEntity;

public interface TestService {
    ResponseEntity<?> saveTemplate(OrderItem orderItem);

    ResponseEntity<?> saveBill(MyOrder myOrder);
}
