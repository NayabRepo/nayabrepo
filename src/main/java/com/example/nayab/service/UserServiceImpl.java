package com.example.nayab.service;

import com.example.nayab.domain.User;
import com.example.nayab.repository.UserRepository;
import com.example.nayab.security.JwtGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtGenerator jwtGenerator;

    @Override
    public ResponseEntity<?> signup(User user) {
        userRepository.save(user);
        return new ResponseEntity("Signup Successfull", HttpStatus.OK);
    }

    @Override
    public Boolean validateUser(User user) {
        Iterable<User> usersList = userRepository.findAll();
        for (User user1 : usersList) {
            if (user.getEmailId().equals(user1.getEmailId())) {
                if (user.getPassword().equals(user1.getPassword())) {
                   return true;
                }
            }
        }
        return false;
    }

    @Override
    public ResponseEntity<?> login(User user) {
        if(validateUser(user)){
            return new ResponseEntity(jwtGenerator.generate(user),HttpStatus.OK);
        }
        return new ResponseEntity("Invalid id or password",HttpStatus.BAD_REQUEST);

    }
}
