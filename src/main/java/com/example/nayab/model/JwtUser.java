package com.example.nayab.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

@Data
public class JwtUser {

    private Long id;
    private String userName;
    private String role;

}
