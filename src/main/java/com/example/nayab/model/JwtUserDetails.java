package com.example.nayab.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class JwtUserDetails implements UserDetails {
    private List<? extends GrantedAuthority> grantedAuthorities;
    private String userName;
    private Long id;
    private String token;

    public JwtUserDetails(String userName, Long id, String token, List<GrantedAuthority> grantedAuthorities) {
        this.userName=userName;
        this.token=token;
        this.id=id;
        this.grantedAuthorities=grantedAuthorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.getPassword() ;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getToken() {
        return token;
    }


    public Long getId() {
        return id;
    }
}

